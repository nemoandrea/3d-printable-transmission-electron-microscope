## 3D Printable Transmission Electron Microscope 🔬 Model 

3D print your own *Transmission Electron Microscope (TEM)*! This model is designed for FDM printing, but probably works okay on SLA printers with some smart placement on the buildplate.

![](media/composite_banner.png)

✨**Features**:

* The microscope column opens up to reveal the inner layout (lenses, sample position) of the TEM
* Column halves connect with click-fit mechanism
* Removable sample holder
* Tilting stage (~70deg limit) (one axis only 😉)
* Fluorescent screen folds upwards

*Electrons not included.*

**⚠️ NOTE: The model is based on a JEOL 1400 TEM, but the project is in no way supported or endorsed by JEOL.**

Model designed in Blender, sliced in PrusaSlicer and printed successfully on Prusa Printers ❤️ (MK3S and MINI+)

The model resembles a [JEOL 1400 TEM](https://www.jeolusa.com/PRODUCTS/Transmission-Electron-Microscopes-TEM/120-kV/JEM-1400Flash), but the model is not dimensionally accurate. The interior lens shape, size and number of coils are *not at all* accurate, and one should not try to read anything into them. The exact polepiece shape is proprietary information that is not publicly available, so the lenses are placed in the correct position (more or less), but the shape is fictional. This [poster by JEOL](https://www.jeolusa.com/DesktopModules/LiveContent/API/Image/Get?mid=4726&eid=4&Type=View&PortalId=2) gives a little bit more background info if you want to look up the individual components.

> If you have any suggestions about the shape of the lenses, or items that could be made more accurate, let me know.

### Remixing

To edit the model, you will need to install Blender, and open the `jeol_1400.blend` file. Suggestions on how to improve the model (maybe some actual engineering diagrams or pictures of the scope taken apart) are very welcome.

### Printing

To print the model, I suggest you have the following colours available

* White/Beige for microscope body ([group_A](./slicer/jeol_1400_prints_group_A.3mf))
* Dark gray/black for metal parts and base ([group_B](./slicer/jeol_1400_prints_group_B.3mf))
* Green for the fluorescent screen (only a single piece, could optionally be printed in either of the other two colours) ([group_C](./slicer/jeol_1400_prints_group_C.3mf))

The [.stl files](./stl) are oriented correctly for printing. Some items will need supports. [Pre-supported files are available for PrusaSlicer](./slicer), but adjust these as needed for your printer's capability.

>  **⚠️ NOTE:** To get the max size that fits on  a Prusa mini (180mm), use a **scale factor of 2160** (units in mm) for the `.stl` files. Adjust for different sized printers.

### 🔎 Support preview

Below are the print times and a suggestion for the supports; scaled to the max size of a Prusa MINI+.

![](media/group_A.JPG)

![](media/group_B.JPG)

![](media/group_C.JPG)